const gulp = require("gulp");
const rename = require("gulp-rename");
const sass = require("gulp-sass")(require("sass"));
const browserSync = require("browser-sync").create();
const cleanCSS = require("gulp-clean-css");

gulp.task("buildStyle", () => {
  return gulp
    .src("./styles/scss/*.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(gulp.dest("./styles/css"));
});

gulp.task("clean-css", () => {
  return gulp
    .src("./styles/css/styles.css")
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(rename("style_min.css"))
    .pipe(gulp.dest("./styles/css/main"));
});
gulp.task("build", gulp.series(["buildStyle", "clean-css"]));

gulp.task("dev", () => {
  browserSync.init({
    server: "./",
  });
  gulp.watch("./styles/scss/**.scss", gulp.series("buildStyle", "clean-css"));
  gulp.watch("./index.html").on("change", browserSync.reload);
  gulp
    .watch("./styles/css/main/style_min.css")
    .on("change", browserSync.reload);
});
